/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_ENABLEDTESTS_HPP
#define OCL_ENABLEDTESTS_HPP

#define TESTCLASS_TESTS_ENABLED 1
#define TESTCOMPARE_TESTS_ENABLED 1
#define TESTCONVERTERUTILITY_TESTS_ENABLED 1
#define TESTINTEGERUTILITY_TESTS_ENABLED 1
#define TESTMEMORYUTILITY_TESTS_ENABLED 1
#define TESTMINMAX_TESTS_ENABLED 1
#define TESTNUMERICUTILITY_TESTS_ENABLED 1
#define TESTSTRING_TESTS_ENABLED 1
#define TESTSTRINGUTILITY_TESTS_ENABLED 1
#define TESTTIME_TESTS_ENABLED 1
#define TESTTYPES_TESTS_ENABLED 1

#endif // OCL_ENABLEDTESTS_HPP
